# Nitpick Styles

Cloned and adapted from ShaneNolan's inspiring Pyton PRoject Template: https://github.com/ShaneNolan/python-project-template

## Getting started

Create .nitpick.toml in your project:
```
[tool.nitpick]
style = "https://gitlab.com/chrigu3014/nitpick-styles/-/raw/main/style.toml"
```
